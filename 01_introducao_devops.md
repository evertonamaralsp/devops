# Introdução DevOps

![Overview](https://pplware.sapo.pt/wp-content/uploads/2017/04/dev-ops.jpg "Overview")

DevOps já é uma palavra hiper a alguns anos e seu objetivo é simples deixar a implantação e desenvolvimento mais rapidos. Hoje temos um dezenas de ferramentas que se propoem a fazer devops, mas o que é devops? o que git, docker, puppet, k8s, ansible, terraforme, jenkins, phometeus, pipeline, SRE entre outras tem em comum para estarem no ecosistema devops?

Alem de um conseito, DevOps é cultura, as tecnilogias de nada adiantam se seu coleginha, chefe, o chefe do chefe ou a empresa não se apossarem das praticas e usarem para tudo, meio devops não é devops. se vc tem uma automação, mas algumas partes tem que ser feitos manual vc não tem devops.

Apesar de forte essa ideia, a reflexão não é sobre perfeccionismo, mas sobre a cultura e pessoas. Na frase anterios completaria com 'se vc não lutar tudo ser automatizado'.

Alem de toda a agenda que vamos discutir durante esses dias, cada topico é uma introdução que pode ser explorando mais por vocês ou ser solicitado para o semantix academy um cursos sobre um ponto expecifico.

## Uma Breve História do DevOps

![Overview](https://2n1ike1orqzb2wbwvw1fb502-wpengine.netdna-ssl.com/wp-content/uploads/2016/08/devops_timeline.jpg "Overview")

- [2001] - Manifesto Agil em 2001 na melhora do processo de desenvolvimento;
- [2008] - Agile Conference 2008 em Toronto-CA quando Patrick Debois, administrador de sistemas, gerente de projetos e especialista em desenvolvimento, propõe discutir métodos sobre como solucionar os eternos conflitos entre as áreas de desenvolvimento e operações de TI
- [2009] - Um ano mais tarde, dois especialistas da Flickr comandaram um seminário chamado “10+ Deploys per Day: Dev and Ops Cooperation at Flickr” na O’Reilly Velocity Conference – Flickr.
- [2009] O impacto desse seminário resultou no primeiro DevOpsDay, em Ghent, na Bélgica.  
- [2011] A Gartner chancela a incorporação do DevOps: Em março de 2011, o Gartner publicou um relatório no qual afirmava que, até o final de 2015, DevOps se tornaria a principal estratégia em 20% das organizações mundiais

## Conseitos DevOps?

![Overview](http://missaodevops.com.br/img/blog/mapa-devops.png "Overview")

### Agile
Iniciamos com as metodologias e frameworks que viabilizam colaboração, entregas rápidas, contínuas, com ritmo constante e sustentável.

### Continuous Integration (CI)
Fail Fast! Lembre-se deste termo! Acredite, quase tudo aqui irá girar em torno disso!
Com a agilidade citada anteriormente, o merge dos códigos é feito com frequência, evitamos o temido 'merge hell'!
Serviço de Integração Contínua irá avaliar qualidade, compilar, testar e sabe qual o ponto mais importante? Dar visão a todo o time!

### Continuous Delivery (CD)
A missão aqui é garantir que podemos entregar nossa aplicação a qualquer momento!
Tudo deve ser rápido, seguro e transparente, o processo de entrega é automatizado e os envolvidos apenas acompanham, tudo em tempo real!
Na prática, você envia, sempre que necessário, um release para produção, apenas apertando um botão!

### Continuous Deployment (CD)
Continuous Deployment remove o passo de deploy manual no pipeline de deploy;
e resulta em múltiplos deploys por dia.

## O que DevOps não é?
* DevOps não é cargo.
* DevOps não é Dev + Ops.
* DevOps não é uma equipe.
* DevOps não é um processo.
* DevOps não é uma ferramenta.
* DevOps não é sinônimo de Entrega Contínua (Continuous Delivery).
* DevOps não é exclusivo para organizações que desenvolvem e entregam software utilizando métodos Ágeis.
* DevOps não é automação do processo de build e deploy (embora a automação de infrastrutura, automação do processo de build e deploy esteja contido dentro da cultura de DevOps).

## Conclusao

Duvidas?
Uma vez que esses temas estão claros bora conhecer as ferramnetas mais usadas para o implentaçãode um DevOps?

Links:

- https://www.mundodevops.com/como-o-devops-surgiu/
- https://gaea.com.br/conheca-a-incrivel-historia-do-devops/
- https://agilecoachninja.wordpress.com/2016/03/14/devops-o-que-nao-e-o-que-e/
