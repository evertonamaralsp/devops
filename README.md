# Modelo e estrutura dos cursos colaborativos:

## [Proposta] Estrutura de pastas do curso no Git:

- Agenda
  - Topicos com media de tempo programado para atividade;
    - Sub Topicos
      - Qdo aplicavel exemplos praticos
      - Qdo aplicavel Exercicios
      - Extra Conhecimentos avançados
      - Extra Exercicios Avançados
    - Qdo possivel Solução dos exercicios avançados;    
- Ppt
- Exercicios
  - Basico/Intermediario
  - Avançado
- Materias extra (programas, scripts, links)
- Apostilas

## [Proposta] Como participar como colaborador nos cursos:

1. Ter vontade;
2. Estudar um assunto;
3. Entrar em contato com a cordenação;
4. Ajudar na manutenção, criação;
5. Sinalizar disposição para ser instrutor(???);
6. Sinalizar disposição para ser instrutor online(???);
7. Respondendo a duvidas em foruns(???);

### Como participar:
* Abrir uma branch;
* Fazer a alteração;
* Abrir Pull Request(PR) com comentario;
* ou Procurar o responsavel(is) pelo curso;

## [Proposta] Classificação dos cursos em 3 categorias:

* Consolidado;  
* Em avaliação;
* Em criação;

###  Consolidado

Aberto para: sugestões, correções, novos exercicios.

### Em avaliação
Aberto para: Mudança de agenda, inclusão exclusão de topicos, sugestões, correções, novos exercicios;

### Em criação
Sugestão de cursos e agenda, mudança de agenda, inclusão exclusão de topicos, sugestões, correções, novos exercicios;

#### Pre-requisitos:
- Criar Plano de aulas:
  - ter instalado docker, VM;
  - ter feitos o curso X ou ter o conhecimentos X,Y,Z;
- Nivel do conteudo:
  - Iniciante, Intermediario, Avançado;
- Objetivo;
- Agenda com: Topico, SubTopicos, tempo aula;
- PPT;
- Exercicios;
- Exemplos Praticos(???);
- Apostila(???).
