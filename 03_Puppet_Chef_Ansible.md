# Puppet, Chef, Ansible

## Apresentações e considerações

A configuração sempre foi uma parte vital da existência de um aplicativo. A partir de um servidor, configurações como apache ou hadoop ou qualquer outra serviço precisamos mexer em vários arquivos de configuração para o seu funcionamento correto.

Como a cultura DevOps está sendo adotada nas indústrias de TI, o interesse na plataforma de automação também se adapta à velocidade e agilidade comumente promovidas como benefícios principais do DevOps e isso significa uma necessidade crescente de profissionais de TI, não apenas dispostos a adotar a mentalidade e a metodologia por trás a abordagem de soluções Devops, mas também capaz de desenvolver habilidades de DevOps com novas ferramentas de software, como Chef, Puppet e Ansible, estão entre as mais destacadas ferramentas de gerenciamento de configuração e automação de implantação. Cada uma dessas ferramentas tem sua força particular. Essa automação e orquestração facilitam a vida de qualquer tamanho de infraestrutura.

## Historia e Fundamentos

No passado, os desenvolvedores e as operações costumavam fazer essa configuração manualmente, e esses processos de configuração manual apresentam várias desvantagens, além de serem o próprio manual. Do SSH ou RDP para cada servidor e adicionar ou editar as configurações, leva muito tempo. Por exemplo, se ocorrer um problema no ambiente de produção, existem 10 a 20 servidores e precisamos fazer uma alteração na configuração do arquivo de configuração do apache. Quanto tempo você acha que vai demorar para resolver o problema? Demora algum tempo para resolver o problema. Por causa das mesmas alterações em cada servidor separadamente. E se houver uma maneira de fazer todas essas coisas com apenas um clique de um botão ou apenas uma linha de comando?

### Para isso hoje existe o CaI

* configurações do servidor
* estrutura de arquivos do servidor
* configurações de aplicativos etc.

Temos a capacidade de automatizar tudo relacionado a configurações. E você pode manter as alterações na configuração de manutenção com versão, o que permite reverter para as versões anteriores sempre que necessário. No CaI, podemos até executar os patches críticos de aplicativos sem problemas, sem afetar o ambiente Live com o Zero Downtime. É incrível o quanto o CaC evoluiu e como podemos alcançar essas coisas com apenas uma linha de código com facilidade. Você não acredita em quão eficientes estes são, desde pequenas alterações na configuração até alterações muito complexas na configuração que podemos escrever como código.

No mundo do DevOps, o CaI agora está se tornando uma metodologia usada com freqüência e todos na indústria de software estão sendo pressionados automaticamente para automatizar o seu produto o máximo possível. (Do desenvolvedor, engenheiro de controle de qualidade, engenheiros de operação, gerentes de projeto ao proprietário do produto, é importante manter todas as configurações automatizadas como CaI.)

Embora existam inúmeras ferramentas de CM disponíveis no mundo do DevOps, vamos discutir as seguintes tecnologias mais destacadas:

* Puppet
* Chef
* Ansible

![Overview](https://miro.medium.com/max/1084/0*nQjI6pRc1omdRbQY.jpg "Overview")

### O Puppet

É uma das ferramentas de automação mais antigas que é muito popular entre a comunidade DevOps. É uma ferramenta de código aberto. Isso tem sido usado em alguns dos maiores e mais exigentes ambientes do mundo. O Puppet é baseado em Ruby, mas usa a linguagem de script de domínio (DSL). É executado como uma arquitetura de mestre/agente. O Puppet possui um sistema CM baseado em pull.

Os agentes gerenciam nós e solicitações de formulários mestres que controlam as informações de configuração. Esses agentes pesquisam relatórios de status e consultas sobre seu nó associado a partir do servidor puppet mestre. O mais importante é que ele roda em quase todos os sistemas operacionais. Também possui suporte da comunidade muito ativo por meio de laboratórios de Puppets. Também possui uma versão Enterprise. Possui a interface da Web da Web mais intuitiva para cuidar de muitas tarefas, incluindo relatórios e gerenciamento de nós em tempo real.

![puppet dashboard](https://zdnet4.cbsistatic.com/hub/i/r/2018/10/08/59399d66-751f-4e91-b5b8-bd19311162dc/resize/770xauto/5706571f6c7be7aa9a48899348cb4985/puppet-insights-full-dashboard.jpg "puppet  dashboard")

### Chef

Chef é uma ferramenta de código aberto usada para o CM e baseada em pull. Normalmente, o chef opera como um modelo de Mestre/agentes. É baseado em Ruby. Para a maior parte dos playbook de receitas que você escreve é em Ruby puro. O servidor Chef é executado na máquina Mestre e o cliente Chef é executado como agentes em cada máquina cliente. Essa configuração é muito estável, confiável e madura, especialmente para implantações em grande escala nos ambientes público e privado. O Chef também funciona com qualquer tipo de sistema operacional. Possui uma coleção muito rica de módulos e receitas de configuração. O Git oferece uma forte capacidade de controle de versão. O Chef fornece documentação, suporte e contribuições sólidos de uma comunidade ativa. A versão do Chef Enterprise possui uma interface rica que pode gerenciar cada uma das configurações de nós e ambientes.

![chef dashboard](http://techgenix.com/tgwordpress/wp-content/uploads/2017/01/Chef-automates-infrastructure.png "chef dashboard")

### Ansible

Ansible é uma ferramenta de código aberto. A plataforma Ansible é escrita em Python e os usuários podem escrever playbook e script usando YAML. Foi desenvolvido para simplificar as tarefas complexas de orquestração e CM. E isso usa a configuração do modelo push. Você pode configurar o ansible em um servidor como servidor de controle, mas não há agentes em execução nas máquinas clientes. Ele usa a conexão SSH para efetuar login nos nós que você deseja configurar. Os nós do cliente precisam apenas de python. Essa é a única dependência necessária nos nós, pois possui fácil instalação e configuração inicial. A falta de mestre elimina pontos de falha e problemas de desempenho. A implantação e a comunicação com menos agente são mais rápidas que o modelo de agente mestre. É mais adequado para ambientes projetados para escalar rapidamente. Fácil curva de aprendizado graças ao uso do YAML. A estrutura do manual é simples e claramente estruturada. Mais importante: Ansible Suporta os modelos push e pull. O Ansible também possui uma versão corporativa chamada Ansible Tower.

![Ansible Tower dashboard](https://nl.devoteam.com/wp-content/thumbnails/uploads/sites/15/2019/04/task-monitoring-ansible-tower-tt-width-919-height-540-fill-0-crop-0-bgcolor-eeeeee-except_gif-1.png "Ansible Tower Dashboard")

## Vantagens e desvantagens

### Puppet
#### Vantagens
- É executado no Linux, CentOS, Debian, Ubuntu, Fedora, BSD, MacOS, Windows e outros.
- Usa sua própria sintaxe "fácil", o Puppet DSL ou Domain Specific Language. Você também pode usar os módulos Ruby personalizados.
- Recursos abrangentes de geração de relatórios
- A grande biblioteca de módulos existentes no Puppet e os módulos são de qualidade mista.
- A interface com o usuário da Web está disponível apenas na versão corporativa
- Arquitetura simples: servidor Puppet e agentes Puppet
- O Puppet fornece uma solução de software eficiente e escalável para gerenciar as configurações de um grande número de dispositivos.
- Os administradores de sistema usam o Puppet para gerenciar recursos de computação, como servidores físicos e virtuais e dispositivos de rede.
- O Puppet é implantado usando um arranjo cliente / servidor, em que o servidor ou o mestre do Puppet gerencia um ou mais nós do cliente.

### Desvantagens
Existem algumas desvantagens do Puppet também. Como se não fosse a melhor solução disponível para escalar implantações. Além disso, o Puppet não possui sistema de envio, portanto, nenhuma ação imediata sobre as alterações. O processo de recebimento segue um agendamento específico para tarefas.

### Chef
#### Vantagens

- Sintaxe abrangente de Ruby para “receitas” de configuração: isso traz uma curva de aprendizado mais alta, mas mais poder;
- A interface do usuário da Web é padrão e gratuita.
- A arquitetura é mais complexa: você precisa de um servidor Chef, de agentes Chef e de uma estação de trabalho Chef para configuração e gerenciamento.
- Grande biblioteca de receitas existentes de qualidade mista.
- Procedimento simples de instalação multiplataforma e independente.
- Desempenho mais rápido.

### Desvantagens
Uma das desvantagens é que a configuração inicial é complicada. Como não é uma ferramenta simples, que pode levar a grandes bases de códigos e ambientes complicados. O chef também não suporta a funcionalidade push.

### Ansible
#### Vantagens

- Ansible é baseado em Python
- A mais nova dessas três tecnologias, portanto, com a menor comunidade
- Funciona sem agentes, mas possui muitas dependências python quando você inicia operações complexas
- Arquitetura simples: tudo o que você precisa é de uma estação de trabalho Ansible e servidores SSH. Sem agentes e sem servidor, mas você tem dependências do Python nas máquinas clientes.
- Interface com o usuário da Web disponível, mas com recursos limitados, pois foi desenvolvido separadamente do projeto principal.
- Scripts e módulos podem ser criados em qualquer idioma, é fornecida uma biblioteca limitada
- Sem suporte para Windows
- sintaxe mais ágil e menor curva de aprendizado
- O Ansible pode usar o Paramiko, uma implementação Python SSH2 ou SSH padrão para comunicações, mas também há um modo de aceleração que permite uma comunicação mais rápida e em maior escala.
- O Ansible possui uma coleção de módulos que podem ser usados para gerenciar vários sistemas, além de infraestrutura de nuvem, como Amazon EC2 e OpenStack

### Desvantagens
Como o Ansible é a nova plataforma entre outras ferramentas de gerenciamento de conteúdo, ele não está totalmente maduro em comparação com o Puppet e o Chef.

## Exercicios com Ansible
