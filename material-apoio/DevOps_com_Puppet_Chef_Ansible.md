# DevOps with Puppet, Chef and Ansible

Como a cultura DevOps está sendo adotada nas indústrias de TI, o interesse na plataforma de automação também se adapta à velocidade e agilidade comumente promovidas como benefícios principais do DevOps e isso significa uma necessidade crescente de profissionais de TI, não apenas dispostos a adotar a mentalidade e a metodologia por trás a abordagem de soluções Devops, mas também capaz de desenvolver habilidades de DevOps com novas ferramentas de software, como Chef, Puppet e Ansible, estão entre as mais destacadas ferramentas de gerenciamento de configuração e automação de implantação. Cada uma dessas ferramentas tem sua força particular. Essa automação e orquestração facilitam a vida de qualquer tamanho de infraestrutura.

Ao escolher o melhor adequado, encontre a ferramenta que melhor funciona para você, porque ela o torna mais produtivo e de qual ferramenta você precisa, depende de seus requisitos e do seu ambiente atual. Ferramentas como Chef, Puppet e Ansible não são necessariamente apenas para DevOps Services, seu uso na organização de TI pode estabelecer as bases para acelerar áreas como entrega de aplicativos, aplicação de patches e assim por diante.

Abaixo está o resumo de alguns pontos fortes de cada ferramenta e algumas coisas que podem influenciá-lo a usar um sobre o outro:

## PorQue Puppet ??

O Puppet é mais lento, mas é o mais amplamente utilizado e suporta múltiplos sistemas operacionais. O Puppet é um sistema de gerenciamento de configuração que fornece uma maneira de definir o estado da infraestrutura de TI e permite a imposição automática do estado correto. Indiscutivelmente, ele desfruta da maior parte da mente dos três. É o mais completo em termos de ações disponíveis, módulos e interface do usuário. O Puppet representa toda a imagem da orquestração do data center, abrangendo praticamente todos os sistemas operacionais e oferecendo ferramentas profundas para os principais sistemas operacionais

- É executado no Linux, CentOS, Debian, Ubuntu, Fedora, BSD, MacOS, Windows e outros.
- Usa sua própria sintaxe "fácil", o Puppet DSL ou Domain Specific Language. Você também pode usar os módulos Ruby personalizados.
- Recursos abrangentes de geração de relatórios
- A grande biblioteca de módulos existentes no Puppet e os módulos são de qualidade mista.
- A interface com o usuário da Web está disponível apenas na versão corporativa
- Arquitetura simples: servidor Puppet e agentes Puppet
- O Puppet fornece uma solução de software eficiente e escalável para gerenciar as configurações de um grande número de dispositivos.
- Os administradores de sistema usam o Puppet para gerenciar recursos de computação, como servidores físicos e virtuais e dispositivos de rede.
- O Puppet é implantado usando um arranjo cliente / servidor, em que o servidor ou o mestre do Puppet gerencia um ou mais nós do cliente.

## Porque Chef ??

O Chef é complexo, mas rápido e poderoso, é uma plataforma de automação que transforma a infraestrutura em código. Muitas empresas de serviços Devops optam por adotar o Chef, porque concedem que tratem seu sistema como objeto, porque com facilidade de uso surge uma falta de robustez. O Chef é semelhante ao Puppet em termos de conceito geral, pois há um servidor mestre e agentes instalados em nós gerenciados, mas difere na implantação real com Devops Solutions & Services.

- Sintaxe abrangente de Ruby para “receitas” de configuração: isso traz uma curva de aprendizado mais alta, mas mais poder;
- A interface do usuário da Web é padrão e gratuita.
- A arquitetura é mais complexa: você precisa de um servidor Chef, de agentes Chef e de uma estação de trabalho Chef para configuração e gerenciamento.
- Grande biblioteca de receitas existentes de qualidade mista.
- Procedimento simples de instalação multiplataforma e independente.
- Desempenho mais rápido.

## Porque Ansible ??

O Ansible é escrito em Python e requer apenas que as bibliotecas Python estejam presentes nos servidores a serem configurados. Ele não suporta Windows, mas é muito fácil, rápido e leve e completamente diferente do Chef e do Puppet. O ponto focal do Ansible deve ser simplificado e rápido, e não requer instalação do agente do nó. Assim, o Ansible executa todas as funções no SSH. Muitos módulos estão disponíveis, desde sistema, banco de dados até gerenciamento em nuvem. É relativamente fácil escrever seus próprios módulos. Muitos idiomas são suportados, mas se você estiver familiarizado com o python, não levará muito tempo para criar seu próprio módulo. Eles suportam todos os serviços principais dos principais provedores de nuvem, como AWS, Rackspace, Digital Ocean, GCE, OpenStack etc.

- Ansible é baseado em Python
- A mais nova dessas três tecnologias, portanto, com a menor comunidade
- Funciona sem agentes, mas possui muitas dependências python quando você inicia operações complexas
- Arquitetura simples: tudo o que você precisa é de uma estação de trabalho Ansible e servidores SSH. Sem agentes e sem servidor, mas você tem dependências do Python nas máquinas clientes.
- Interface com o usuário da Web disponível, mas com recursos limitados, pois foi desenvolvido separadamente do projeto principal.
- Scripts e módulos podem ser criados em qualquer idioma, é fornecida uma biblioteca limitada
- Sem suporte para Windows
- sintaxe mais ágil e menor curva de aprendizado
- O Ansible pode usar o Paramiko, uma implementação Python SSH2 ou SSH padrão para comunicações, mas também há um modo de aceleração que permite uma comunicação mais rápida e em maior escala.
- O Ansible possui uma coleção de módulos que podem ser usados para gerenciar vários sistemas, além de infraestrutura de nuvem, como Amazon EC2 e OpenStack

## Puppet or Chef or Ansible???

- Puppet e Chef ajudam desenvolvedores e organizações orientadas para o desenvolvimento, onde o Ansible é muito mais flexível às necessidades dos administradores de sistema
- A interface simples e a usabilidade do Ansible se encaixam diretamente na psicologia do administrador do sistema e, em uma organização com muitos sistemas Linux e Unix, o Ansible é rápido e fácil de executar, comparativamente.
- O Puppet é o mais maduro e conveniente dos três do ponto de vista da usabilidade, no entanto, é altamente recomendável um sólido conhecimento de Ruby.
- O Puppet não é tão simples quanto o Ansible, e sua configuração pode se tornar complexa às vezes. É a maneira mais segura para ambientes diversos, mas você pode achar que o Ansible se encaixa melhor em uma infraestrutura maior ou mais semelhante.
- O Chef tem um layout muito estável e bem projetado e, embora não esteja exatamente no nível do Puppet em termos de recursos brutos, é uma solução muito capaz. O chef pode representar a curva de aprendizado mais difícil para os administradores que não possuem uma experiência significativa em programação, mas pode ser o ajuste mais lógico para a pessoa com espírito de desenvolvimento e a organização de desenvolvimento.

## Conclusao

Se você examinar cada uma dessas três ferramentas em profundidade, explorar seu design e função e determinar que, enquanto algumas pontuam mais que outras, há um lugar para cada uma se encaixar, dependendo dos objetivos da implantação. Aqui não temos uma preferência universal de uma por outra. Cada um é um bom ajuste em determinadas circunstâncias.


# Link
- https://medium.com/@cuelogicTech/devops-with-puppet-chef-and-ansible-8e471e06abe
