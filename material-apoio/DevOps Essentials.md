# DevOps Essentials

## detalhes do curso
Este curso fornece uma introdução básica ao conceito de DevOps. Ele visa fornecer ao aluno um conhecimento prático do que é o DevOps e como ele afeta a TI no mundo real. Este curso explica a cultura do DevOps, bem como algumas das principais práticas e ferramentas associadas ao DevOps. Ele também apresenta ao aluno a estreita relação entre o DevOps e a nuvem.

Esta seção o familiarizará com o curso do DevOps Essentials e apresentará o conceito do DevOps.

Programa de Estudos
### Introdução
Sobre o Autor do Curso 00:00:27
Recursos e ferramentas do curso 00:02:39
O que é o DevOps? 00:03:37
Uma Breve História do DevOps 00:05:19
QUIZ: Introdução ao DevOps 00:15:00

### Cultura DevOps
Os objetivos do DevOps 00:08:18
Uma história de DevOps vs. silos tradicionais 00:11:29
QUIZ: Cultura do DevOps 00:15:00

### Conceitos e práticas do DevOps
Automação de compilação 00:04:39
Integração contínua 00:04:53
Entrega Contínua e Implantação Contínua 00:06:09
Infraestrutura como código 00:04:33
Gerenciamento de configurações 00:04:33
Orquestração 00:05:32
Monitoramento 00:04:43
Microsserviços 00:07:30
QUIZ: Conceitos e práticas do DevOps 00:15:00
O pipeline do DevOps 00:30:00

### Ferramentas do DevOps
Introdução às ferramentas do DevOps 00:04:58
Ferramentas para automação de compilação e integração contínua 00:04:36
Ferramentas para gerenciamento de configuração 00:06:29
Ferramentas para virtualização e conteinerização 00:03:09
Ferramentas para monitoramento 00:04:49
Ferramentas para orquestração 00:04:31
QUIZ: Ferramentas do DevOps 00:15:00

### DevOps e a nuvem
DevOps e a nuvem 00:06:45
DevOps e Google Cloud Platform 00:03:39
DevOps e Microsoft Azure 00:02:22
DevOps e Amazon Web Services 00:04:25
QUIZ: DevOps e a nuvem  00:15:00

### Próximos passos
Próximos passos 00:02:05
