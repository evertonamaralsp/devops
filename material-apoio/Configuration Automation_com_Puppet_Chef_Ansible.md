Configuration Automation with Ansible, Puppet and Chef
Iruka Avantha

Como sabemos, a configuração é parte vital da existência de um aplicativo. A partir de um servidor, configurações como apache ou tomcat ou qualquer outra configuração e vários arquivos de configuração relacionados ao aplicativo são necessários para a existência do aplicativo.

No passado, os desenvolvedores e as operações costumavam fazer essa configuração manualmente, e esses processos de configuração manual apresentam várias desvantagens, além de serem o próprio manual. Do SSH ou RDP para cada servidor e adicionar ou editar as configurações, leva muito tempo. Por exemplo, se ocorrer um problema no ambiente de produção, existem 10 a 20 servidores e precisamos fazer uma alteração na configuração do arquivo de configuração do apache. Quanto tempo você acha que vai demorar para resolver o problema? Demora algum tempo para resolver o problema. Por causa das mesmas alterações em cada servidor separadamente. E se houver uma maneira de fazer todas essas coisas com apenas um clique de um botão ou apenas uma linha de comando?

Sim, é claro que há uma maneira de fazer isso automatizando as configurações específicas como código (CaC). O CaC acompanha o processo que conhecemos como infraestrutura como código. Basicamente, o CaC inclui

- configurações do servidor
- estrutura de arquivos do servidor
- configurações de aplicativos etc.

Temos a capacidade de automatizar tudo relacionado a configurações. E você pode manter as alterações na configuração de manutenção com versão, o que permite reverter para as versões anteriores sempre que necessário. No CaC, podemos até executar os patches críticos de aplicativos sem problemas, sem afetar o ambiente Live com o Zero Downtime. É incrível o quanto o CaC evoluiu e como podemos alcançar essas coisas com apenas uma linha de código com facilidade. Você não acredita em quão eficientes estes são, desde pequenas alterações na configuração até alterações muito complexas na configuração que podemos escrever como código

No mundo do DevOps, o CaC agora está se tornando uma metodologia usada com freqüência e todos na indústria de software estão sendo pressionados automaticamente para automatizar o seu produto o máximo possível. (Do desenvolvedor, engenheiro de controle de qualidade, engenheiros de operação, gerentes de projeto ao proprietário do produto, é importante manter todas as configurações automatizadas como CaC.)

Devops Life cycle é uma coleção de engrenagens que funcionam continuamente quando automatizamos todos os processos. Nesta coleção de engrenagens, o CM se torna uma parte crucial. Se essa engrenagem quebrar, pode afetar todo o sistema que está sendo desenvolvido. Quando automatizamos o CaC, resolvemos esse problema trabalhando continuamente nessa engrenagem sem quebrar. Por exemplo, alguém faz uma alteração simples na permissão de arquivo em um servidor que afeta a funcionalidade do aplicativo. Com o CaC, essa alteração pode ser aplicada a vários ambientes em alguns segundos e voltará ao normal como era anteriormente. A idéia é transformar qualquer engenheiro de Devps em alguém que irá,

´´´
Decida o que é necessário e "faça assim".
-Jean-Luc Picard (oficial da Frota Estelar na série Star Trek)
Decide what is required and “make it so.”
-Jean-Luc Picard (Starfleet officer in the Star Trek series)
´´´

Isso é particularmente importante à medida que as organizações avançam para uma abordagem de DevOps com base na entrega contínua (CD). A entrega contínua permite um fluxo contínuo de novas funcionalidades incrementais para implementação, com o DevOps agilizando o movimento dessas cargas de trabalho atualizadas de maneira incremental dos ambientes de desenvolvimento e teste para as operações. Atualizações rápidas e sincronização do DevOps não são possíveis quando a equipe de Operação dimensiona e configura manualmente o ambiente de produção.

No mundo DevOps, os engenheiros devem ser capazes de provisionar um ambiente de trabalho bem definido e testado usando as ferramentas de CM junto com as ferramentas de CD do CI. CFEngine, Juju, Leme, Sal, Chef, Fantoche, Ansible são algumas das ferramentas de CM em uso. No entanto, espera-se que essas ferramentas atendam a várias funcionalidades, como a idempotência.
Como exemplo de idempotência, se multiplicarmos algum valor por zero, o valor será zero. Não importa quantas vezes você o multiplique, o valor ainda será zero. Em outras palavras, idempotência significa que é executável sem erros.

Um sistema CM idempotente garante que esses problemas não ocorram. Em essência, os sistemas idempotentes conhecem o estado desejado e mantêm essa configuração perpetuamente. O CM idempotente oferece aos administradores a capacidade de executar repetidamente um conjunto de ações com o mesmo resultado. Como um recurso, a menos que não haja garantia de que haverá o mesmo estado desejado em todos os ambientes provisionados até a produção. E isso ainda permite a falta de cargas de trabalho repetíveis e portabilidade entre Data Centers para Cloud ou Cloud to Data Centers.

Embora existam inúmeras ferramentas de CM disponíveis no mundo do DevOps, vamos discutir as seguintes tecnologias mais destacadas,

- Puppet
- Chef
- Ansible

Essas ferramentas são fornecidas de maneira diferente conforme os requisitos de negócios.Vamos ver o que cada uma dessas ferramentas fornece de maneira diferente,

O Puppet [1] é uma das ferramentas de automação mais antigas que é muito popular entre a comunidade DevOps. É uma ferramenta de código aberto. Isso tem sido usado em alguns dos maiores e mais exigentes ambientes do mundo. O Puppet é baseado em Ruby, mas usa a linguagem de script de domínio (DSL). É executado como uma arquitetura de agente mestre. E o Puppet possui um sistema CM baseado em tração.

Os agentes gerenciam nós e solicitações de formulários mestres que controlam as informações de configuração. Esses agentes pesquisam relatórios de status e consultas sobre seu nó associado a partir do servidor Puppet mestre. O mais importante é que ele roda em quase todos os sistemas operacionais. Também possui suporte da comunidade muito ativo por meio de laboratórios de Puppets. Também possui uma versão Enterprise. Possui a interface da Web da Web mais intuitiva para cuidar de muitas tarefas, incluindo relatórios e gerenciamento de nós em tempo real.

imagens01-configuracao_p_c_a.png

Existem algumas desvantagens do Puppet também. Como se não fosse a melhor solução disponível para escalar implantações. Além disso, o Puppet não possui sistema de envio, portanto, nenhuma ação imediata sobre as alterações. O processo de recebimento segue um agendamento específico para tarefas.

Chef [2] também é uma ferramenta de código aberto usada para o CM. Essa também é uma ferramenta CM baseada em pull. Normalmente, o chef opera como um modelo de cliente principal. É baseado em Ruby. Para a maior parte dos livros de receitas que você escreve, Ruby puro é usado. O servidor Chef é executado na máquina principal e o cliente Chef é executado como agentes em cada máquina cliente. Essa configuração é muito estável, confiável e madura, especialmente para implantações em grande escala nos ambientes público e privado. O Chef também funciona com qualquer tipo de sistema operacional. Possui uma coleção muito rica de módulos e receitas de configuração. O Git oferece uma forte capacidade de controle de versão. O Chef fornece documentação, suporte e contribuições sólidos de uma comunidade ativa. A versão do Chef Enterprise possui uma interface rica que pode gerenciar cada uma das configurações de nós e ambientes.

imagens02-configuracao_p_c_a.png

Uma das desvantagens é que a configuração inicial é complicada. Como não é uma ferramenta simples, que pode levar a grandes bases de códigos e ambientes complicados. O chef também não suporta a funcionalidade push.

Ansible [3] também é uma ferramenta de código aberto. A plataforma Ansible é escrita em Python e os usuários podem escrever playbook e script usando YAML. Foi desenvolvido para simplificar as tarefas complexas de orquestração e CM. E isso usa a configuração do modelo push. Você pode configurar o ansible em um servidor como servidor de controle, mas não há agentes em execução nas máquinas clientes. Ele usa a conexão SSH para efetuar login nos nós que você deseja configurar. Os nós do cliente precisam apenas de python. Essa é a única dependência necessária nos nós, pois possui fácil instalação e configuração inicial. A falta de mestre elimina pontos de falha e problemas de desempenho. A implantação e a comunicação com menos agente são mais rápidas que o modelo de agente mestre. É mais adequado para ambientes projetados para escalar rapidamente. Fácil curva de aprendizado graças ao uso do YAML. A estrutura do manual é simples e claramente estruturada. Mais importante: Ansible Suporta os modelos push e pull. O Ansible também possui uma versão corporativa chamada Ansible Tower.

imagens03-configuracao_p_c_a.png

Como o Ansible é a nova plataforma entre outras ferramentas de gerenciamento de conteúdo, ele não está totalmente maduro em comparação com o Puppet e o Chef.

Agora vamos comparar essas coisas em geral,

imagens04-configuracao_p_c_a.png.png

Concluindo, como você pode ver acima, Puppet e Chef são os jogadores antigos no mundo do DevOps, tornando-os mais adequados para empresas maiores. O Ansible é um novo player nesse campo e uma boa opção para soluções rápidas e simples. De qualquer maneira, o Ansible parece promissor com o uso dele no mundo do DevOps. Cada uma dessas ferramentas de CM é direcionada a diferentes tipos de usuários com o mesmo mundo do DevOps. Quando escolhem uma ferramenta para sua solução de CM, devem considerar requisitos exclusivos para seus fluxos de trabalho, considerando o mais viável para suas organizações. Considere principalmente que tipo de arquitetura, modelo de operação e entre outros aspectos técnicos e de negócios essenciais. Entregar uma das ferramentas CaC ao seu produto melhora a qualidade, a estabilidade e a mudança para as mais recentes tecnologias e recursos.

# Link:
- https://medium.com/@Iruka.Avantha/configuration-automation-with-ansible-puppet-and-chef-191b928a5583
